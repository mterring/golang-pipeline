#!/bin/bash -e

source /build_environment.sh

if [ -d buildreport ]; then
  rm -rf buildreport/*
else
  mkdir buildreport
fi

novendor_dirs=$(go list ./... | grep -v '/vendor/')

echo "--------------------------------------"
echo "Using nonvendor dirs:"
echo "$novendor_dirs"

echo "--------------------------------------"
echo "* Run tests with race detector"
go test -race ${novendor_dirs}

# Run test coverage on each subdirectories and merge the coverage profile.
echo "--------------------------------------"
echo "* Building coverage report"

echo "mode: count" > buildreport/profile.cov
coverDirs=$novendor_dirs
for dir in $coverDirs
do
    path="$GOPATH/src/$dir"
    if ls $path/*.go &> /dev/null; then
        go test -covermode=count -coverprofile=$path/profile.tmp $dir
        if [ -f $path/profile.tmp ]
        then
            cat $path/profile.tmp | tail -n +2 >> buildreport/profile.cov
            rm $path/profile.tmp
        fi
    fi
done

go tool cover -html buildreport/profile.cov -o buildreport/cover.html
