#!/bin/bash -e

source /build_environment.sh

for pkg in ${pkgName[@]}
do
    # Grab the last segment from the package name
    name=${pkg##*/}
    echo "--------------------------------------"
    echo "* Building Go binary: $pkg"

    flags=(-a -installsuffix cgo)
    ldflags=('-s -X main.version='$BUILD_VERSION)

    # Compile statically linked version of package
    # see https://golang.org/cmd/link/ for all ldflags
    CGO_ENABLED=${CGO_ENABLED:-0} go build \
        "${flags[@]}" \
        -ldflags "${ldflags[@]}" \
        -o "$goPath/src/$pkg/$name" \
        "$pkg"

    if [[ $COMPRESS_BINARY == "true" ]];
    then
      goupx $name
    fi

    if [ -e "/var/run/docker.sock" ] && [ -e "$goPath/src/$pkg/Dockerfile" ];
    then

        # Default TAG_NAME to package name if not set explicitly
        tagName=${tagName:-"$name":latest}
        echo "--------------------------------------"
        echo "* Building Docker image: $tagName"

        # Build the image from the Dockerfile in the package directory
        docker build --pull -t $tagName .
    fi
done
