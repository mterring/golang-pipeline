#!/bin/bash -e

source /build_environment.sh

if [ -d buildreport ]; then
  rm -rf buildreport/*
else
  mkdir buildreport
fi

novendor_dirs=$(go list ./... | grep -v '/vendor/')

echo "--------------------------------------"
echo "Using nonvendor dirs:"
echo "$novendor_dirs"

echo "--------------------------------------"
echo "* Run vet + golint"
for f in go_vet.txt golint.txt
do
    touch buildreport/${f}
done

for d in $novendor_dirs
do
    go vet ${d} 2>> buildreport/go_vet.txt || true
    golint ${d} >> buildreport/golint.txt || true
done

echo "--------------------------------------"
echo "* Run errcheck"
errcheck ${novendor_dirs} > buildreport/errcheck.txt || true
